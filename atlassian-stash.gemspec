# -*- encoding: utf-8 -*-
# stub: atlassian-stash 0.7.0 ruby lib

Gem::Specification.new do |s|
  s.name = "atlassian-stash"
  s.version = "0.7.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Seb Ruiz"]
  s.date = "2015-11-05"
  s.description = "Provides convenient functions for interacting with Bitbucket Server through the command line"
  s.email = "sruiz@atlassian.com"
  s.executables = ["stash"]
  s.extra_rdoc_files = ["LICENSE.txt", "README.md"]
  s.files = [".document", "Gemfile", "LICENSE.txt", "README.md", "Rakefile", "VERSION", "bin/stash", "lib/atlassian/stash/git.rb", "lib/atlassian/stash/pull_request.rb", "lib/atlassian/stash/repo_info.rb", "lib/atlassian/stash/version.rb", "lib/atlassian/util/text_util.rb", "lib/stash_cli.rb", "test/helper.rb", "test/test_stash-create-pull-request.rb", "test/test_stash-git.rb", "test/test_stash-repo-info.rb", "test/test_text-util.rb"]
  s.homepage = "https://bitbucket.org/atlassian/stash-command-line-tools"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.5.1"
  s.summary = "Command line tools for Bitbucket Server"

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<git>, [">= 1.2.5"])
      s.add_runtime_dependency(%q<json>, [">= 1.7.5"])
      s.add_runtime_dependency(%q<commander>, ["~> 4.3.0"])
      s.add_runtime_dependency(%q<launchy>, ["~> 2.4.2"])
      s.add_development_dependency(%q<shoulda>, [">= 0"])
      s.add_development_dependency(%q<rdoc>, ["~> 3.12"])
      s.add_development_dependency(%q<bundler>, [">= 1.2.0"])
      s.add_development_dependency(%q<jeweler>, ["~> 2.0.0"])
      s.add_development_dependency(%q<rcov>, [">= 0"])
      s.add_development_dependency(%q<simplecov>, [">= 0"])
      s.add_development_dependency(%q<minitest>, [">= 0"])
      s.add_development_dependency(%q<mocha>, [">= 0"])
    else
      s.add_dependency(%q<git>, [">= 1.2.5"])
      s.add_dependency(%q<json>, [">= 1.7.5"])
      s.add_dependency(%q<commander>, ["~> 4.3.0"])
      s.add_dependency(%q<launchy>, ["~> 2.4.2"])
      s.add_dependency(%q<shoulda>, [">= 0"])
      s.add_dependency(%q<rdoc>, ["~> 3.12"])
      s.add_dependency(%q<bundler>, [">= 1.2.0"])
      s.add_dependency(%q<jeweler>, ["~> 2.0.0"])
      s.add_dependency(%q<rcov>, [">= 0"])
      s.add_dependency(%q<simplecov>, [">= 0"])
      s.add_dependency(%q<minitest>, [">= 0"])
      s.add_dependency(%q<mocha>, [">= 0"])
    end
  else
    s.add_dependency(%q<git>, [">= 1.2.5"])
    s.add_dependency(%q<json>, [">= 1.7.5"])
    s.add_dependency(%q<commander>, ["~> 4.3.0"])
    s.add_dependency(%q<launchy>, ["~> 2.4.2"])
    s.add_dependency(%q<shoulda>, [">= 0"])
    s.add_dependency(%q<rdoc>, ["~> 3.12"])
    s.add_dependency(%q<bundler>, [">= 1.2.0"])
    s.add_dependency(%q<jeweler>, ["~> 2.0.0"])
    s.add_dependency(%q<rcov>, [">= 0"])
    s.add_dependency(%q<simplecov>, [">= 0"])
    s.add_dependency(%q<minitest>, [">= 0"])
    s.add_dependency(%q<mocha>, [">= 0"])
  end
end

